//Import Command//

import java.util.Scanner;

public class VirtualPetApp {
	public static void main(String[] args) { //Main Method//
		
		//Connecting to the keyboard//
		
		Scanner keyboard = new Scanner(System.in);
		
		Panda[] embarrassmentOfPanda =	new Panda[2];
		
		for(int i = 0; i < embarrassmentOfPanda.length; i++) {
			System.out.println("Enter the origin:");
			String location = keyboard.nextLine();
			
			System.out.println("Enter the population:");
			int count = keyboard.nextInt();
			keyboard.nextLine();
		
			System.out.println("Enter the lifespan:");
			double age = keyboard.nextDouble();
			keyboard.nextLine();
			
			embarrassmentOfPanda[i] = new Panda(location, count, age);
		}
		
		//step 13 - Print BEFORE the call
		
		System.out.println("Step 13: print all fields BEFORE:");
		System.out.println(embarrassmentOfPanda[embarrassmentOfPanda.length-1].getOrigin());
		System.out.println(embarrassmentOfPanda[embarrassmentOfPanda.length-1].getPopulation());
		System.out.println(embarrassmentOfPanda[embarrassmentOfPanda.length-1].getLifespan());

		//step 12 - Lab4B instruction
		
		System.out.println("Please choose another origin for the last panda:");
		String newLocation = keyboard.nextLine();
		embarrassmentOfPanda[embarrassmentOfPanda.length-1].setOrigin(newLocation);

		//step 13 - Print AFTER the call
		
		System.out.println("Step 13: Print all fields AFTER:");
		System.out.println(embarrassmentOfPanda[embarrassmentOfPanda.length-1].getOrigin());
		System.out.println(embarrassmentOfPanda[embarrassmentOfPanda.length-1].getPopulation());
		System.out.println(embarrassmentOfPanda[embarrassmentOfPanda.length-1].getLifespan());
	}
}
