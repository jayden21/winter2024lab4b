public class Panda {
	
	//Attributes//
	
	private String origin;
	private int population;
	private double lifespan;
	
	//Constructor//
	
	public Panda(String origin, int population, double lifespan) {
		this.origin = origin;
		this.population = population;
		this.lifespan = lifespan;
	}
	
	//Getter 
	
	public String getOrigin(){
		return this.origin;
	}
	public int getPopulation(){
		return this.population;
	}
	public double getLifespan(){
		return this.lifespan;
	}
	
	//Setter
	
	public void setOrigin(String newOrigin){
		this.origin = newOrigin;
	}

	//Creating the first method//
	
	public String myIntro() {
		return "Welcome to Panda Club House! We're a species found in "
				+ origin + ". We've a population of "
				+ population +
				" Our life span is " + lifespan + " years";
	}
	
	//Creating the second method//
	
	public void newLocation() {
		this.origin = "Shaanxi";
	}
}
